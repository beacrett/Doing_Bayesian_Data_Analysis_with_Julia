### Doing Bayesian Data Analysis with Julia

I set up this project as I started working through _Doing Bayesian Data Analysis_
and translating its R code to Julia.  The repo has a jupyter notebook, containing
Julia code examples, for each chapter I worked through.  Hopefully this is helpful
for others and I welcome any notes about bugs or errors (as I am learning both
Julia and Bayesian methods, I'm sure there'll be a few).